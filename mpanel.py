#!/usr/bin/env python3

import gi, datetime, os, subprocess, cairo, locale, dbus, json
from datetime import datetime
import requests

gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")
gi.require_version('GtkLayerShell', '0.1')
from gi.repository import Gtk, Gdk, GtkLayerShell, GObject, GLib, GdkPixbuf, Gio, Pango, DbusmenuGtk3
from dbus.mainloop.glib import DBusGMainLoop

from wl_framework.loop_integrations import GLibIntegration
from wl_framework.network.connection import WaylandConnection
from wl_framework.protocols.foreign_toplevel import ForeignTopLevel

icon_text = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="64"
   height="64"
   version="1"
   id="svg12"
   sodipodi:docname="System.svg"
   inkscape:version="0.92.4 (5da689c313, 2019-01-14)">
  <metadata
	 id="metadata18">
	<rdf:RDF>
	  <cc:Work
		 rdf:about="">
		<dc:format>image/svg+xml</dc:format>
		<dc:type
		   rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
		<dc:title></dc:title>
	  </cc:Work>
	</rdf:RDF>
  </metadata>
  <defs
	 id="defs16" />
  <sodipodi:namedview
	 pagecolor="#ffffff"
	 bordercolor="#666666"
	 borderopacity="1"
	 objecttolerance="10"
	 gridtolerance="10"
	 guidetolerance="10"
	 inkscape:pageopacity="0"
	 inkscape:pageshadow="2"
	 inkscape:window-width="1908"
	 inkscape:window-height="1044"
	 id="namedview14"
	 showgrid="false"
	 inkscape:zoom="3.6875"
	 inkscape:cx="-14.915254"
	 inkscape:cy="32"
	 inkscape:window-x="4"
	 inkscape:window-y="28"
	 inkscape:window-maximized="0"
	 inkscape:current-layer="svg12" />
  <path
	 style="opacity:0.2"
	 d="M 30.599609,5 C 27.497209,5 25,7.4972093 25,10.599609 v 2.625 a 21,21 0 0 0 -6.623047,3.826172 l -2.275391,-1.3125 c -2.686757,-1.5512 -6.09919,-0.635976 -7.6503901,2.050781 l -1.4003907,2.421876 c -1.5511999,2.686758 -0.6359758,6.09919 2.0507813,7.65039 l 2.2656255,1.308594 A 21,21 0 0 0 11,33 a 21,21 0 0 0 0.367188,3.830078 l -2.2656255,1.308594 c -2.6867571,1.5512 -3.6019813,4.963633 -2.0507813,7.65039 l 1.4003907,2.421876 c 1.5512001,2.686757 4.9636331,3.601981 7.6503901,2.050781 l 2.259766,-1.304688 A 21,21 0 0 0 25,52.769531 v 2.63086 C 25,58.502791 27.497209,61 30.599609,61 h 2.800782 C 36.502791,61 39,58.502791 39,55.400391 v -2.640625 a 21,21 0 0 0 6.623047,-3.810547 l 2.275391,1.3125 c 2.686757,1.5512 6.09919,0.635977 7.65039,-2.050781 l 1.400391,-2.421876 c 1.5512,-2.686757 0.635975,-6.09919 -2.050781,-7.65039 L 52.632812,36.830078 A 21,21 0 0 0 53,33 21,21 0 0 0 52.632812,29.169922 l 2.265626,-1.308594 c 2.686757,-1.5512 3.601981,-4.963632 2.050781,-7.65039 l -1.400391,-2.421876 c -1.5512,-2.686758 -4.963633,-3.601981 -7.65039,-2.050781 l -2.259766,1.304688 A 21,21 0 0 0 39,13.228516 V 10.599609 C 39,7.4972093 36.502791,5 33.400391,5 Z"
	 id="path2" />
  <path
	 style="fill:#546e7a"
	 d="M 30.599609,4 C 27.497209,4 25,6.4972094 25,9.5996094 v 2.6249996 a 21,21 0 0 0 -6.623047,3.826172 l -2.275391,-1.3125 c -2.686757,-1.5512 -6.09919,-0.635976 -7.6503901,2.050781 l -1.4003907,2.421876 c -1.5511999,2.686758 -0.6359758,6.09919 2.0507813,7.65039 l 2.2656255,1.308594 A 21,21 0 0 0 11,32 a 21,21 0 0 0 0.367188,3.830078 l -2.2656255,1.308594 c -2.6867571,1.5512 -3.6019813,4.963633 -2.0507813,7.65039 l 1.4003907,2.421876 c 1.5512001,2.686757 4.9636331,3.601981 7.6503901,2.050781 l 2.259766,-1.304688 A 21,21 0 0 0 25,51.769531 v 2.63086 C 25,57.502791 27.497209,60 30.599609,60 h 2.800782 C 36.502791,60 39,57.502791 39,54.400391 v -2.640625 a 21,21 0 0 0 6.623047,-3.810547 l 2.275391,1.3125 c 2.686757,1.5512 6.09919,0.635976 7.65039,-2.050781 l 1.400391,-2.421876 c 1.5512,-2.686757 0.635975,-6.09919 -2.050781,-7.65039 L 52.632812,35.830078 A 21,21 0 0 0 53,32 21,21 0 0 0 52.632812,28.169922 l 2.265626,-1.308594 c 2.686757,-1.5512 3.601981,-4.963632 2.050781,-7.65039 l -1.400391,-2.421876 c -1.5512,-2.686758 -4.963634,-3.601981 -7.65039,-2.050781 l -2.259766,1.304688 A 21,21 0 0 0 39,12.228516 V 9.5996094 C 39,6.4972094 36.502791,4 33.400391,4 Z"
	 id="path4" />
  <path
	 style="opacity:0.1;fill:#ffffff"
	 d="M 30.599609,4 C 27.497209,4 25,6.4972094 25,9.5996094 V 10.599609 C 25,7.4972094 27.497209,5 30.599609,5 h 2.800782 C 36.502791,5 39,7.4972094 39,10.599609 V 9.5996094 C 39,6.4972094 36.502791,4 33.400391,4 Z M 25,12.224609 a 21,21 0 0 0 -6.623047,3.826172 l -2.275391,-1.3125 c -2.686757,-1.5512 -6.09919,-0.635976 -7.6503901,2.050781 l -1.4003907,2.421876 c -0.6033566,1.045044 -0.8286088,2.200037 -0.7265624,3.3125 0.069736,-0.789767 0.3027074,-1.578362 0.7265624,-2.3125 l 1.4003907,-2.421876 c 1.5512001,-2.686757 4.9636331,-3.601981 7.6503901,-2.050781 l 2.275391,1.3125 A 21,21 0 0 1 25,13.224609 Z m 14,0.0039 v 1 a 21,21 0 0 1 6.638672,3.814453 l 2.259766,-1.304688 c 2.686757,-1.5512 6.09919,-0.635977 7.65039,2.050781 l 1.400391,2.421876 c 0.423855,0.734138 0.656827,1.522733 0.726562,2.3125 0.102046,-1.112463 -0.123206,-2.267456 -0.726562,-3.3125 l -1.400391,-2.421876 c -1.5512,-2.686758 -4.963633,-3.601981 -7.65039,-2.050781 l -2.259766,1.304688 A 21,21 0 0 0 39,12.228516 Z M 52.767578,29.09179 52.632812,29.16992 A 21,21 0 0 1 52.974609,32.541016 21,21 0 0 0 53,32 21,21 0 0 0 52.767578,29.091797 Z m -41.529297,0.0039 A 21,21 0 0 0 11,32 a 21,21 0 0 0 0.02539,0.458984 21,21 0 0 1 0.341797,-3.289062 z m 41.523438,6.808594 a 21,21 0 0 1 -0.128907,0.925781 l 2.265626,1.308594 c 1.641712,0.947843 2.617034,2.590267 2.777343,4.33789 0.185479,-2.100574 -0.824725,-4.210545 -2.777343,-5.33789 z m -41.529297,0.0039 -2.1308595,1.230469 c -1.9526187,1.127345 -2.9628223,3.237316 -2.7773437,5.33789 0.1603097,-1.747623 1.1356309,-3.390047 2.7773437,-4.33789 l 2.2656255,-1.308594 a 21,21 0 0 1 -0.134766,-0.921875 z"
	 id="path6" />
  <path
	 style="opacity:0.2"
	 d="m 32.000237,21.00088 c 6.627221,0 11.999676,5.372455 11.999676,11.999676 0,6.627221 -5.372455,11.999676 -11.999676,11.999676 -6.627221,0 -11.999676,-5.372455 -11.999676,-11.999676 0,-6.627221 5.372455,-11.999676 11.999676,-11.999676 z"
	 id="path8" />
  <path
	 style="fill:#ffffff"
	 d="m 32.000237,20.001334 c 6.627221,0 11.999676,5.372455 11.999676,11.999676 0,6.627221 -5.372455,11.999676 -11.999676,11.999676 -6.627221,0 -11.999676,-5.372455 -11.999676,-11.999676 0,-6.627221 5.372455,-11.999676 11.999676,-11.999676 z"
	 id="path10" />
</svg>"""

translate = {"tr":{0:"Panelden Kaldır",
					1:"Panele Sabitle",
					2:"Çalıştır",
					3:"Tekrar Çalıştır",
					4:"Ekranı Kapla",
					5:"Küçült",
					6:"Tam Ekran",
					7:"Kapat",
					8:"Kapat",
					9:"Yeniden Başlat",
					10:"Uykuya Al",
					11:"Çık",
					12:"Ekranı Kilitle"},
			"en":{0:"UnPinned",
					1:"Pinned",
					2:"Exec",
					3:"Exec New",
					4:"Maximize",
					5:"Minimize",
					6:"Fullscreen",
					7:"Close",
					8:"Shutdown",
					9:"Reboot",
					10:"Sleep",
					11:"Logout",
					12:"Lock Screen"}
			}

l = locale.getdefaultlocale()
l = l[0].split("_")[0]
locales = list(translate.keys())
if l not in locales:
	l = "en"
_ = translate[l]

ayguci_port = 5005

mpanel_settings = os.path.expanduser("~/.config/mpanel.ini")

settings = {"position":"bottom",
			"space":10,
			"w_space":2,
			"icon_size":22,
			"line_size":1,
			"systray_size":100,
			"show_all_monitors":1,
			"clock_format":"%H:%M",
			"group_tasks":0,
			"notification_char_size" : 40,
			"notification_font" : "DejaVu Sans Mono 12",
			"notification_time_out": 5,
			"notification_save": 1,
			"widgets_left":["menu","showdesktop","tasks"],
			"widgets_right":["screenshot","notifications","sound","backlight","battery","bluetooth","wifi","clock","systray","exit"],
			"pinned_apps":["firefox-default"]}

app_id_icon = {"sakura":"lxterminal",
				"lxtask":"gnome-system-monitor",
				"meg":"gnome-screenshot",
				"gimp-2.99":"gimp",
				"sudoui":"gnome-encfs-manager",
				"RUR":"gnome-encfs-manager",
				"gpartedbin":"gparted",
				"resimlik":"image-viewer",
				"mservice":"/usr/milis/mservice/icons/mservice.svg",
				"ayguci ui":"/usr/milis/ayguciui/aux/ayguci.svg",
				"uget-gtk":"uget",
				"mps_ui":"/usr/milis/mpsui/mps_icons/paket.svg",
				"connman-gtk":"preferences-system-network",
				"org.qt-project.qtcreator":"qtcreator",
				"io.github.celluloid_player.Celluloid":"io.github.Celluloid",
				"org.inkscape.Inkscape":"inkscape"}

exec_change = {"libreoffice-writer":"libreoffice --writer",
			"libreoffice-calc":"libreoffice --calc",
			"libreoffice-draw":"libreoffice --draw",
			"libreoffice-base":"libreoffice --base",
			"libreoffice-impress":"libreoffice --impress",
			"libreoffice-math":"libreoffice --math",
			"ayguci ui":"/usr/milis/ayguciui/aux/run-gui.sh",
			"mps_ui":"sudoui -c /usr/milis/mpsui/mps_ui.sh",
			"audacity":"env LD_LIBRARY_PATH=/usr/lib/audacity UBUNTU_MENUPROXY=0 audacity",
			"mservice":"sudoui -c /usr/milis/mservice/mservice.py",}


########################################################################
########################################################################
class Context(GObject.Object, WaylandConnection):
	def __init__(self):
		GObject.Object.__init__(self)
		WaylandConnection.__init__(self, eventloop_integration=GLibIntegration())
		self.add_signal('periodic_update', tuple())
		self.add_signal('wayland_sync')
		self.add_timer(2, self.on_periodic_update)
		self.manager = TaskManager(self)

	def add_signal(self, signal_name, signal_args=None):
		if signal_args is None:
			signal_args = (GObject.TYPE_PYOBJECT,)
		GObject.signal_new(
			signal_name, self,
			GObject.SignalFlags.RUN_LAST,
			GObject.TYPE_PYOBJECT, signal_args
		)

	def on_initial_sync(self, data):
		super().on_initial_sync(data)
		self.seat = self.display.seat
		self.emit('wayland_sync', self)

	def on_periodic_update(self):
		self.emit('periodic_update')

class ToplevelManager(ForeignTopLevel):
	def __init__(self, wl_connection, context):
		super().__init__(wl_connection)
		self.context = context

	def on_toplevel_created(self, toplevel):
		self.context.emit('toplevel_new', toplevel)

	def on_toplevel_synced(self, toplevel):
		self.context.emit('toplevel_synced', toplevel)

	def on_toplevel_closed(self, toplevel):
		self.context.emit('toplevel_closed', toplevel)
########################################################################
########################################################################
class TaskManager:
	def __init__(self, context):
		context.add_signal('toplevel_new')
		context.add_signal('toplevel_synced')
		context.add_signal('toplevel_closed')
		context.connect('wayland_sync', self.on_wl_sync)
		self.context = context
		self.manager = None

	def on_wl_sync(self, context, wl_connection):
		self.manager = ToplevelManager(wl_connection, context)

	def app_toggle(self, *args):
		toplevel = args[-1]
		if 'activated' in toplevel.states:
			toplevel.set_minimize(True)
		else:
			self.app_activate(toplevel)

	def app_activate(self, *args):
		toplevel = args[-1]
		toplevel.activate(self.context.seat)

	def app_minimize(self, *args):
		toplevel = args[-1]
		if 'minimized' in toplevel.states:
			return
		toplevel.set_minimize(True)

	def app_toggle_minimize(self, *args):
		toplevel = args[-1]
		toplevel.set_minimize('minimized' not in toplevel.states)

	def app_toggle_maximize(self, *args):
		toplevel = args[-1]
		toplevel.set_maximize('maximized' not in toplevel.states)

	def app_toggle_fullscreen(self, *args):
		toplevel = args[-1]
		toplevel.set_fullscreen('fullscreen' not in toplevel.states)

	def app_close(self, *args):
		toplevel = args[-1]
		toplevel.close()
########################################################################
########################################################################
class MPanel(Gtk.Window):
	def __init__(self,SIZE):
		super().__init__()
		self.SIZE = SIZE
		self.task_state = {}
		if settings["position"] == "left" or settings["position"] == "right":
			m_box = Gtk.VBox()
		else:
			m_box = Gtk.HBox()
		self.add(m_box)
		for widget in settings["widgets_left"]:
			m_box.pack_start(*self.get_widget(widget),settings["w_space"])
		right_modules = settings["widgets_right"]
		for widget in reversed(right_modules):
			m_box.pack_end(*self.get_widget(widget),settings["w_space"])

	def set_btn_background(self,box,btn):
		color = box.get_style_context().get_background_color(Gtk.StateFlags.NORMAL)
		btn.override_background_color(Gtk.StateFlags.NORMAL, color)
		btn.set_border_width(0)

	def get_widget(self,widget_name):
		if widget_name == "showdesktop":
			showdesktop_btn = Gtk.Button()
			pb =  get_icon_in_theme("org.xfce.panel.showdesktop",settings["icon_size"])
			image = Gtk.Image.new_from_pixbuf(pb)
			showdesktop_btn.set_image(image)
			showdesktop_btn.connect("clicked",self.showdesktop_btn_clicked)
			return (showdesktop_btn,0,0)
		elif widget_name == "screenshot":
			screenshot_btn = Gtk.Button()
			pb =  get_icon_in_theme("gnome-screenshot",settings["icon_size"])
			image = Gtk.Image.new_from_pixbuf(pb)
			screenshot_btn.set_image(image)
			screenshot_btn.connect("clicked",self.screenshot_btn_clicked)
			return (screenshot_btn,0,0)
		elif widget_name == "notifications":
			self.notif_btn = Gtk.Button()
			if settings["position"] == "left" or settings["position"] == "right":
				self.notif_btn.set_image_position(Gtk.PositionType.TOP)
			pb =  get_icon_in_theme("preferences-desktop-notification-bell",settings["icon_size"])
			image = Gtk.Image.new_from_pixbuf(pb)
			self.notif_btn.set_image(image)
			self.notif_btn.connect("clicked",self.notifications_btn_clicked)
			return (self.notif_btn,0,0)
		elif widget_name == "exit":
			if settings["position"] == "top" or settings["position"] == "bottom":
				box = Gtk.HBox()
			else:
				box = Gtk.VBox()

			exit_btn = Gtk.Button()
			pb =  get_icon_in_theme("system-shutdown-panel",settings["icon_size"])
			image = Gtk.Image.new_from_pixbuf(pb)
			exit_btn.set_image(image)
			exit_btn.connect("clicked",self.exit_btn_clicked)
			box.pack_start(exit_btn,0,0,0)

			self.lock_button = Gtk.Button()
			self.set_btn_background(box,self.lock_button)
			self.lock_button.set_tooltip_text(_[12])
			self.lock_button.connect("clicked",self.exit_run,"gtklock")
			self.lock_button.set_no_show_all(True)
			pb =  get_icon_in_theme("xfce-system-lock",settings["icon_size"])
			image = Gtk.Image.new_from_pixbuf(pb)
			self.lock_button.set_image(image)
			box.pack_start(self.lock_button,0,0,0)

			self.logout_button = Gtk.Button()
			self.set_btn_background(box,self.logout_button)
			self.logout_button.set_tooltip_text(_[11])
			self.logout_button.connect("clicked",self.exit_run,"logout.sh")
			self.logout_button.set_no_show_all(True)
			pb =  get_icon_in_theme("gnome-logout",settings["icon_size"])
			image = Gtk.Image.new_from_pixbuf(pb)
			self.logout_button.set_image(image)
			box.pack_start(self.logout_button,0,0,0)

			self.sleep_button = Gtk.Button()
			self.set_btn_background(box,self.sleep_button)
			self.sleep_button.set_tooltip_text(_[10])
			self.sleep_button.connect("clicked",self.exit_run,"suspend.sh")
			self.sleep_button.set_no_show_all(True)
			pb =  get_icon_in_theme("sleep",settings["icon_size"])
			image = Gtk.Image.new_from_pixbuf(pb)
			self.sleep_button.set_image(image)
			box.pack_start(self.sleep_button,0,0,0)

			self.reboot_button = Gtk.Button()
			self.set_btn_background(box,self.reboot_button)
			self.reboot_button.set_tooltip_text(_[9])
			self.reboot_button.connect("clicked",self.exit_run,"sudo /bin/reboot")
			self.reboot_button.set_no_show_all(True)
			pb =  get_icon_in_theme("system-reboot",settings["icon_size"])
			image = Gtk.Image.new_from_pixbuf(pb)
			self.reboot_button.set_image(image)
			box.pack_start(self.reboot_button,0,0,0)

			self.shutdown_button = Gtk.Button()
			self.set_btn_background(box,self.shutdown_button)
			self.shutdown_button.set_tooltip_text(_[8])
			self.shutdown_button.connect("clicked",self.exit_run,"sudo /bin/poweroff")
			self.shutdown_button.set_no_show_all(True)
			pb =  get_icon_in_theme("system-shutdown",settings["icon_size"])
			image = Gtk.Image.new_from_pixbuf(pb)
			self.shutdown_button.set_image(image)
			box.pack_start(self.shutdown_button,0,0,0)
			return (box,0,0)
		elif widget_name == "menu":
			menu_btn = Gtk.Button()
			pb =  get_icon_in_theme("gnome-tracker",settings["icon_size"])
			image = Gtk.Image.new_from_pixbuf(pb)
			menu_btn.set_image(image)
			menu_btn.connect("clicked",self.menu_btn_run)
			return (menu_btn,0,0)
		elif widget_name == "sound":
			ad1 = Gtk.Adjustment(value=40, lower=0, upper=100, step_incr=1, page_incr=10, page_size=0)
			if settings["position"] == "top" or settings["position"] == "bottom":
				box = Gtk.HBox()
				self.sound_scale = Gtk.Scale(orientation=Gtk.Orientation.HORIZONTAL, adjustment=ad1)
				self.sound_scale.set_size_request(150,-1)
				self.sound_scale.set_value_pos(Gtk.PositionType.LEFT)
			else:
				box = Gtk.VBox()
				self.sound_scale = Gtk.Scale(orientation=Gtk.Orientation.VERTICAL, adjustment=ad1)
				self.sound_scale.set_size_request(-1,150)
				self.sound_scale.set_value_pos(Gtk.PositionType.BOTTOM)
			self.sound_btn = Gtk.Button()
			self.sound_btn.connect("clicked",self.sound_btn_clicked)
			box.pack_start(self.sound_btn,0,0,0)
			self.mute_unmute_button = Gtk.Button()
			self.set_btn_background(box,self.mute_unmute_button)
			self.mute_unmute_button.connect("clicked",self.mute_unmute_change)
			self.mute_unmute_button.set_no_show_all(True)
			box.pack_start(self.mute_unmute_button,0,0,0)
			self.mixer_button = Gtk.Button()
			self.set_btn_background(box,self.mixer_button)
			self.mixer_button.connect("clicked",self.open_mixer)
			self.mixer_button.set_no_show_all(True)
			pb =  get_icon_in_theme("gnome-alsamixer-icon",settings["icon_size"])
			image = Gtk.Image.new_from_pixbuf(pb)
			self.mixer_button.set_image(image)
			box.pack_start(self.mixer_button,0,0,0)
			self.sound_scale.set_no_show_all(True)
			self.sound_scale.set_slider_size_fixed(True)
			self.sound_scale.connect("change_value",self.sound_scale_change)
			self.sound_scale.set_digits(0)
			box.pack_start(self.sound_scale,0,0,0)
			return (box,0,0)
		elif widget_name == "backlight":
			ad1 = Gtk.Adjustment(value=40, lower=0, upper=100, step_incr=1, page_incr=10, page_size=0)
			if settings["position"] == "top" or settings["position"] == "bottom":
				box = Gtk.HBox()
				self.backlight_scale = Gtk.Scale(orientation=Gtk.Orientation.HORIZONTAL, adjustment=ad1)
				self.backlight_scale.set_size_request(150,-1)
				self.backlight_scale.set_value_pos(Gtk.PositionType.LEFT)
			else:
				box = Gtk.VBox()
				self.backlight_scale = Gtk.Scale(orientation=Gtk.Orientation.VERTICAL, adjustment=ad1)
				self.backlight_scale.set_size_request(-1,150)
				self.backlight_scale.set_value_pos(Gtk.PositionType.BOTTOM)
			self.backlight_btn = Gtk.Button()
			self.backlight_btn.connect("clicked",self.backlight_btn_clicked)
			box.pack_start(self.backlight_btn,0,0,0)
			self.backlight_scale.set_no_show_all(True)
			self.backlight_scale.set_slider_size_fixed(True)
			self.backlight_scale.connect("change_value",self.backlight_scale_change)
			self.backlight_scale.set_digits(0)
			box.pack_start(self.backlight_scale,0,0,0)
			return (box,0,0)
		elif widget_name == "battery":
			if settings["position"] == "top" or settings["position"] == "bottom":
				box = Gtk.HBox()
			else:
				box = Gtk.VBox()
			self.battery_btn = Gtk.Button()
			self.battery_btn.connect("clicked",self.battery_btn_clicked)
			box.pack_start(self.battery_btn,0,0,0)
			self.battery_lbl = Gtk.Label()
			self.battery_lbl.set_justify(Gtk.Justification.CENTER)
			self.battery_lbl.set_no_show_all(True)
			box.pack_start(self.battery_lbl,0,0,0)
			return (box,0,0)
		elif widget_name == "wifi":
			if settings["position"] == "top" or settings["position"] == "bottom":
				box = Gtk.HBox()
			else:
				box = Gtk.VBox()
			self.wifi_btn = Gtk.Button()
			self.wifi_btn.connect("clicked",self.wifi_btn_clicked)
			box.pack_start(self.wifi_btn,0,0,0)
			self.wifi_lbl = Gtk.Button()
			self.set_btn_background(box,self.wifi_lbl)
			self.wifi_lbl.connect("clicked",self.wifi_lbl_clicked)
			self.wifi_lbl.set_no_show_all(True)
			box.pack_start(self.wifi_lbl,0,0,0)
			return (box,0,0)
		elif widget_name == "tasks":
			self.tasks_store = Gtk.ListStore(GdkPixbuf.Pixbuf,str,str)
			self.tasks_iv = Gtk.IconView(model=self.tasks_store)
			self.tasks_iv.connect("button_press_event", self.tasks_iv_click)
			self.tasks_iv.set_margin(0)
			self.tasks_iv.set_pixbuf_column(0)
			self.tasks_iv.set_tooltip_column(2)
			scroll = Gtk.ScrolledWindow()
			if settings["position"] == "top" or settings["position"] == "bottom":
				scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.NEVER)
			else:
				scroll.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
			scroll.add(self.tasks_iv)
			return (scroll,1,1)
		elif widget_name == "clock":
			self.clock_btn = Gtk.Label()
			self.clock_btn.set_has_window(True)
			self.clock_btn.set_events(Gdk.EventMask.BUTTON_PRESS_MASK)
			self.clock_btn.connect("button-press-event", self.clock_btn_click)
			return (self.clock_btn,0,0)
		elif widget_name == "bluetooth":
			if settings["position"] == "top" or settings["position"] == "bottom":
				box = Gtk.HBox()
			else:
				box = Gtk.VBox()
			self.blue_btn = Gtk.Button()
			self.blue_btn.connect("clicked",self.blue_btn_clicked)
			box.pack_start(self.blue_btn,0,0,0)
			self.blue_on_off = Gtk.Button()
			self.set_btn_background(box,self.blue_on_off)
			self.blue_on_off.connect("clicked",self.blue_on_off_clicked)
			box.pack_start(self.blue_on_off,0,0,0)
			self.blue_on_off.set_no_show_all(True)
			self.blue_mix = Gtk.Button()
			self.set_btn_background(box,self.blue_mix)
			pb =  get_icon_in_theme("bluetooth",settings["icon_size"])
			image = Gtk.Image.new_from_pixbuf(pb)
			self.blue_mix.set_image(image)
			self.blue_mix.connect("clicked",self.blue_mix_clicked)
			self.blue_mix.set_no_show_all(True)
			box.pack_start(self.blue_mix,0,0,0)
			return (box,0,0)
		elif widget_name == "systray":
			self.systray_store = Gtk.ListStore(GdkPixbuf.Pixbuf,str,str)
			self.systray_iv = Gtk.IconView(model=self.systray_store)
			self.systray_iv.connect("button_press_event", self.systray_iv_click)
			self.systray_iv.set_margin(0)
			self.systray_iv.set_pixbuf_column(0)
			return (self.systray_iv,0,1)

	def showdesktop_btn_clicked(self,widget):
		all_minimized = True
		t_levels = []
		for task in top_levels:
			if type(task) == str:
				pass
			elif type(task) == list:
				t_levels += task
			else:
				t_levels.append(task)
		for task in t_levels:
			if "minimized" not in task.states:
				all_minimized = False
		if not all_minimized:
			for task in t_levels:
				context.manager.app_minimize(task)
				self.task_state[task] = task.states
		else:
			active = None
			for task in t_levels:
				if task in self.task_state.keys():
					states = self.task_state[task]
					if "activated" in states:
						active = task
					elif "maximized" in states:
						context.manager.app_toggle_maximize(task)
					elif "fullscreen" in states:
						context.manager.app_toggle_fullscreen(task)
					elif "minimized" in states:
						pass
					else:
						context.manager.app_toggle_minimize(task)
			if active != None:
				context.manager.app_activate(active)
	
	def screenshot_btn_clicked(self,widget):
		subprocess.Popen([os.path.expanduser("~/.config/swappy/area.sh")])
	
	def exit_run(self,widget,command):
		subprocess.Popen(command.split())

	def exit_btn_clicked(self,widget):
		if self.logout_button.is_visible():
			self.lock_button.hide()
			self.logout_button.hide()
			self.sleep_button.hide()
			self.reboot_button.hide()
			self.shutdown_button.hide()
		else:
			self.lock_button.show()
			self.logout_button.show()
			self.sleep_button.show()
			self.reboot_button.show()
			self.shutdown_button.show()

	def blue_mix_clicked(self,widget):
		subprocess.Popen(["blueman-manager"])

	def blue_on_off_clicked(self,widget):
		subprocess.Popen(["bluetoothctl", "power",widget.get_label()])

	def clock_btn_click(self,widget,text):
		subprocess.Popen(["osmo"])

	def update_clock(self):
		now = datetime.now()
		c = now.strftime(settings["clock_format"])
		if settings["position"] == "top" or settings["position"] == "bottom":
			self.clock_btn.set_text(c)
		else:
			self.clock_btn.set_label(c.replace(":","\n"))

	def systray_iv_click(self,widget,event):
		model = self.systray_iv.get_model()
		path = self.systray_iv.get_path_at_pos(event.x, event.y)
		if event.type == Gdk.EventType.BUTTON_PRESS:
			if path != None:
				if event.button == 1 or event.button == 3:
					item = model[path]
					menu = DbusmenuGtk3.Menu.new(item[1],item[2])
					menu.set_size_request(250,200)
					menu.attach_to_widget(widget)
					menu.show_all()
					menu.popup_at_pointer()

	def tasks_iv_click(self,widget,event):
		global top_levels
		model = self.tasks_iv.get_model()
		path = self.tasks_iv.get_path_at_pos(event.x, event.y)
		if event.type == Gdk.EventType.BUTTON_PRESS:
			if path != None:
				top_level = top_levels[int(str(path))]
				if event.button == 1:
					if type(top_level) == str:
						self.exec_app(None,top_level)
					elif type(top_level) == list:
						menu = Gtk.Menu()
						for top in top_level:
							menu_item = Gtk.MenuItem(label=top.title)
							menu_item.connect("activate",self.toogle_list_app,top)
							menu.add(menu_item)
						menu.attach_to_widget(self.tasks_iv)
						menu.show_all()
						menu.popup_at_pointer()
					else:
						context.manager.app_toggle(top_level)
				elif event.button == 3:
						menu = Gtk.Menu()
						if type(top_level) == str:
							unp_menu_item = Gtk.MenuItem(label=_[0])
							unp_menu_item.connect("activate",self.unpinned_app,top_level)
							menu.add(unp_menu_item)
							ex_menu_item = Gtk.MenuItem(label=_[2])
							ex_menu_item.connect("activate",self.exec_app,top_level)
							menu.add(ex_menu_item)
						else:
							if type(top_level) == list:
								ex_menu_item = Gtk.MenuItem(label=_[3])
								ex_menu_item.connect("activate",self.exec_app,top_level[0].app_id)
								menu.add(ex_menu_item)
							elif top_level.app_id in settings["pinned_apps"]:
								unp_menu_item = Gtk.MenuItem(label=_[0])
								unp_menu_item.connect("activate",self.unpinned_app,top_level.app_id)
								menu.add(unp_menu_item)
							else:
								p_menu_item = Gtk.MenuItem(label=_[1])
								p_menu_item.connect("activate",self.pinned_app,top_level.app_id)
								menu.add(p_menu_item)
							if type(top_level) != list:
								ex_menu_item = Gtk.MenuItem(label=_[3])
								ex_menu_item.connect("activate",self.exec_app,top_level.app_id)
								menu.add(ex_menu_item)
								maximize_menu_item = Gtk.MenuItem(label=_[4])
								maximize_menu_item.connect("activate",self.maximize_menu_app,top_level)
								menu.add(maximize_menu_item)
								minimize_menu_item = Gtk.MenuItem(label=_[5])
								minimize_menu_item.connect("activate",self.miniimize_menu_app,top_level)
								menu.add(minimize_menu_item)
								fullscreen_menu_item = Gtk.MenuItem(label=_[6])
								fullscreen_menu_item.connect("activate",self.fullscreen_menu_app,top_level)
								menu.add(fullscreen_menu_item)
								close_menu_item = Gtk.MenuItem(label=_[7])
								close_menu_item.connect("activate",self.close_app,top_level)
								menu.add(close_menu_item)
						menu.attach_to_widget(self.tasks_iv)
						menu.show_all()
						menu.popup_at_pointer()

	def toogle_list_app(self,widget,top_level):
		context.manager.app_toggle(top_level)

	def fullscreen_menu_app(self,widget,top_level):
		context.manager.app_toggle_fullscreen(top_level)

	def miniimize_menu_app(self,widget,top_level):
		context.manager.app_toggle_minimize(top_level)

	def maximize_menu_app(self,widget,top_level):
		context.manager.app_toggle_maximize(top_level)

	def close_app(self,widget,top_level):
		context.manager.app_close(top_level)

	def exec_app(self,widget,top_level):
		if top_level in exec_change.keys():
			top_level = exec_change[top_level]
		try:
			subprocess.Popen(top_level.split())
		except:
			try:
				top_level = top_level.lower()
				if "-" in top_level:
					subprocess.Popen([top_level.split("-")[0]])
				if "." in top_level:
					subprocess.Popen([top_level.split(".")[-1]])
			except:
				print("#### {} başlatılamadı.".format(top_level))

	def pinned_app(self,widget,top_level):
		settings["pinned_apps"].append(top_level)
		write_settings(None,None)

	def unpinned_app(self,widget,top_level):
		global top_levels
		settings["pinned_apps"].remove(top_level)
		if top_level in top_levels:
			top_levels.remove(top_level)
			update_panel_tasks()
			write_settings(None,None)

	def update_bluetooth(self, info):
		if info == "yes":
			icon_name = "bluetooth-active"
			self.blue_on_off.set_label("off")
		else:
			icon_name = "bluetooth-disabled"
			self.blue_on_off.set_label("on")
		pb =  get_icon_in_theme(icon_name,settings["icon_size"])
		image = Gtk.Image.new_from_pixbuf(pb)
		self.blue_btn.set_image(image)

	def update_wifi(self,info):
		if type(info) == tuple:
			self.wifi_lbl.set_label(info[0])
			if info[1] == 100:
				icon_name = "network-wireless-connected-100"
			elif info[1] > 75:
				icon_name = "network-wireless-connected-75"
			elif info[1] > 50:
				icon_name = "network-wireless-connected-50"
			elif info[1] > 25:
				icon_name = "network-wireless-connected-25"
			else:
				icon_name = "network-wireless-connected-00"
		else:
			icon_name = "notification-network-wireless-disconnected"
			self.wifi_lbl.set_label(info)
		pb =  get_icon_in_theme(icon_name,settings["icon_size"])
		image = Gtk.Image.new_from_pixbuf(pb)
		self.wifi_btn.set_image(image)

	def update_notifications(self,info):
		self.notif_btn.set_label(info)
		
	def wifi_lbl_clicked(self,widget):
		subprocess.Popen(["connman-gtk"])

	def blue_btn_clicked(self,widget):
		if self.blue_on_off.is_visible():
			self.blue_on_off.hide()
			self.blue_mix.hide()
		else:
			self.blue_on_off.show()
			self.blue_mix.show()

	def wifi_btn_clicked(self,widget):
		if self.wifi_lbl.is_visible():
			self.wifi_lbl.hide()
		else:
			self.wifi_lbl.show()

	def update_battery(self,info):
		if info[1]:
			charging = "+"
		else:
			charging = "-"
		if settings["position"] == "top" or settings["position"] == "bottom":
			self.battery_lbl.set_text("{} %{}".format(charging,info[0]))
		else:
			self.battery_lbl.set_text("  {}  \n%{} ".format(charging,info[0]))
		if info[0] == 100:
			icon_name = "battery-100{}"
		elif info[0] > 90:
			icon_name = "battery-090{}"
		elif info[0] > 80:
			icon_name = "battery-080{}"
		elif info[0] > 70:
			icon_name = "battery-070{}"
		elif info[0] > 60:
			icon_name = "battery-060{}"
		elif info[0] > 50:
			icon_name = "battery-050{}"
		elif info[0] > 40:
			icon_name = "battery-040{}"
		elif info[0] > 30:
			icon_name = "battery-030{}"
		elif info[0] > 20:
			icon_name = "battery-020{}"
		elif info[0] > 10:
			icon_name = "battery-010{}"
		else:
			icon_name = "battery-000{}"
		if info[1]:
			icon_name = icon_name.format("-charging")
		else:
			icon_name = icon_name.format("")
		pb =  get_icon_in_theme(icon_name,settings["icon_size"])
		image = Gtk.Image.new_from_pixbuf(pb)
		self.battery_btn.set_image(image)

	def notifications_btn_clicked(self,widget):
		subprocess.Popen(["swaync-client","-t","-sw"])

	def battery_btn_clicked(self,widget):
		if self.battery_lbl.is_visible():
			self.battery_lbl.hide()
		else:
			self.battery_lbl.show()

	def backlight_btn_clicked(self,widget):
		if self.backlight_scale.is_visible():
			self.backlight_scale.hide()
		else:
			self.backlight_scale.show()

	def backlight_scale_change(self,widget,scroll,value):
		subprocess.Popen(["/usr/bin/bash", "-c","light -S {} && light -O".format(int(value))])

	def sound_scale_change(self,widget,scroll,value):
		subprocess.Popen(["pactl","set-sink-volume","0","{}%".format(int(value))])

	def mute_unmute_change(self, widget):
		subprocess.Popen(["pactl","set-sink-mute","0","toggle"])

	def open_mixer(self,widget):
		subprocess.Popen(["pavucontrol"])

	def sound_btn_clicked(self,widget):
		if self.mixer_button.is_visible():
			self.mixer_button.hide()
			self.mute_unmute_button.hide()
			self.sound_scale.hide()
		else:
			self.mixer_button.show()
			self.mute_unmute_button.show()
			self.sound_scale.show()

	def update_backlight(self,info):
		if info > 75:
			icon_name = "notification-display-brightness-full"
		elif info > 50:
			icon_name = "notification-display-brightness-high"
		elif info > 25:
			icon_name = "notification-display-brightness-medium"
		else:
			icon_name = "notification-display-brightness-low"
		self.backlight_scale.set_value(info)
		pb =  get_icon_in_theme(icon_name,settings["icon_size"])
		image = Gtk.Image.new_from_pixbuf(pb)
		self.backlight_btn.set_image(image)

	def update_sound(self,info):
		if info[1]:
			icon_name = "audio-volume-muted"
		elif info[0] > 66:
			icon_name = "audio-volume-high"
		elif info[0] > 33:
			icon_name = "audio-volume-medium"
		else:
			icon_name = "audio-volume-low"
		self.sound_scale.set_value(info[0])
		pb =  get_icon_in_theme(icon_name,settings["icon_size"])
		image = Gtk.Image.new_from_pixbuf(pb)
		self.sound_btn.set_image(image)

		if not info[1]:
			icon_name = "notification-audio-volume-muted"
		elif info[0] > 66:
			icon_name = "audio-volume-high"
		elif info[0] > 33:
			icon_name = "audio-volume-medium"
		else:
			icon_name = "audio-volume-low"
		pb =  get_icon_in_theme(icon_name,settings["icon_size"])
		image = Gtk.Image.new_from_pixbuf(pb)
		self.mute_unmute_button.set_image(image)			


	def menu_btn_run(self,windget):
		subprocess.Popen(["menu.py"])

	def menu_destroy(self,widget):
		self.menu_is_visible = False
		return True

########################################################################
########################################################################
def get_icon_in_theme(icon_name, icon_size = False):
	if icon_name in app_id_icon.keys():
		icon_name = app_id_icon[icon_name]
	else:
		icon_name = icon_name.lower()
	if not icon_size:
		icon_size = settings["icon_size"]
	try:
		icon = icon_theme.load_icon(icon_name, icon_size, Gtk.IconLookupFlags.FORCE_SIZE)
		loader = GdkPixbuf.PixbufLoader()
		loader.set_size(icon_size,icon_size)
		loader.write(icon_text.encode())
		loader.close()
		i = loader.get_pixbuf()
		icon.composite(i,0,0,icon_size,icon_size,0,0,1,1,GdkPixbuf.InterpType.HYPER,125)
		return icon
	except:
		if os.path.exists(icon_name):
			try:
				icon =  GdkPixbuf.Pixbuf.new_from_file_at_size(icon_name,icon_size,icon_size)
				return icon
			except:
				pass
		else:
			i_name = icon_name.split(".")
			for i_n in i_name:
				try:
					icon = icon_theme.load_icon(i_n, icon_size, Gtk.IconLookupFlags.FORCE_SIZE)
					loader = GdkPixbuf.PixbufLoader()
					loader.set_size(icon_size,icon_size)
					loader.write(icon_text.encode())
					loader.close()
					i = loader.get_pixbuf()
					icon.composite(i,0,0,icon_size,icon_size,0,0,1,1,GdkPixbuf.InterpType.HYPER,125)
					return icon
				except:
					pass					
	loader = GdkPixbuf.PixbufLoader()
	loader.set_size(icon_size,icon_size)
	loader.write(icon_text.encode())
	loader.close()
	icon = loader.get_pixbuf()
	return icon

def on_toplevel_new(context, toplevel):
	pass

def update_panel_tasks():
	global top_levels
	for panel in panel_wins:
		panel.tasks_store.clear()
		for top_level in top_levels:
			if type(top_level) == str:
				pb = get_icon_in_theme(top_level)
				panel.tasks_store.append([pb,top_level,top_level])
			elif type(top_level) == list:
				hover = ""
				for tl in top_level:
					hover += tl.title
					hover += "\n"
				pb = get_icon_in_theme(top_level[0].app_id)
				pb = draw_dot(pb)
				panel.tasks_store.append([pb,top_level[0].app_id,hover[:-1]])
			else:
				pb = get_icon_in_theme(top_level.app_id)
				pb = draw_dot(pb)
				panel.tasks_store.append([pb,top_level.app_id,top_level.title])
	if settings["position"] == "top" or settings["position"] == "bottom":
		panel.tasks_iv.set_columns(len(top_levels))
	else:
		panel.tasks_iv.set_columns(1)

def draw_dot(pixbuf):
	if settings["position"] == "bottom" or settings["position"] == "top":
		w = pixbuf.get_width()
		h = pixbuf.get_height()+settings["line_size"]
	if settings["position"] == "left" or settings["position"] == "right":
		w = pixbuf.get_width()+settings["line_size"]
		h = pixbuf.get_height()
	surface = cairo.ImageSurface(pixbuf.get_colorspace(), w, h)
	cr = cairo.Context(surface)
	if settings["position"] == "bottom" or settings["position"] == "right":
		Gdk.cairo_set_source_pixbuf(cr, pixbuf, 0, 0)
	elif settings["position"] == "top":
		Gdk.cairo_set_source_pixbuf(cr, pixbuf, 0, settings["line_size"])
	elif settings["position"] == "left":
		Gdk.cairo_set_source_pixbuf(cr, pixbuf, settings["line_size"], 0)
	cr.paint()
	cr.set_source_rgb(1, 1, 1)
	if settings["position"] == "bottom":
		cr.rectangle(0, pixbuf.get_height(), pixbuf.get_width(), settings["line_size"])
	elif settings["position"] == "top":
		cr.rectangle(0, 0, pixbuf.get_width(), settings["line_size"])
	elif settings["position"] == "left":
		cr.rectangle(0, 0, settings["line_size"], pixbuf.get_height())
	elif settings["position"] == "right":
		cr.rectangle(pixbuf.get_width(), 0, settings["line_size"], pixbuf.get_height())
	cr.fill()
	pixbuf = Gdk.pixbuf_get_from_surface(surface,0,0, w, h)
	return pixbuf

def on_toplevel_synced(context, toplevel):
	global top_levels
	if "tasks" in settings["widgets_left"] or "tasks" in settings["widgets_right"]:
		if toplevel.app_id in top_levels:
			top_index = top_levels.index(toplevel.app_id)
			top_levels[top_index] = toplevel
			update_panel_tasks()
		if toplevel not in top_levels:
			is_add = False
			for top in top_levels:
				if type(top) == str:
					continue
				elif type(top) == list:
					if top[0].app_id == toplevel.app_id:
						if toplevel not in top:
							top.append(toplevel)
							is_add = True
							break
						else:
							is_add = True
							break
				elif top.app_id == toplevel.app_id and settings["group_tasks"]:
					ind = top_levels.index(top)
					top_levels[ind] = [top,toplevel]
					is_add = True
					break
			if not is_add:
				top_levels.append(toplevel)
			update_panel_tasks()

		if "activated" in toplevel.states:
			for panel in panel_wins:
				model = panel.tasks_iv.get_model()
				if model != None:
					i = 0
					for top in top_levels:
						if type(top) == str:
							pass
						elif type(top) == list:
							if toplevel in top:
								iter = model[i]
								panel.tasks_iv.select_path(iter.path)
								break
						elif top == toplevel:
							iter = model[i]
							panel.tasks_iv.select_path(iter.path)
							break
						i += 1
		if "minimized" in toplevel.states:
			for panel in panel_wins:
				panel.tasks_iv.unselect_all()

def on_toplevel_closed(context, toplevel):
	global top_levels
	if "tasks" in settings["widgets_left"] or "tasks" in settings["widgets_right"]:
		if toplevel.app_id in settings["pinned_apps"]:
			try:
				top_index = top_levels.index(toplevel)
				if not settings["group_tasks"]:
					app_id = 0
					for top in top_levels:
						if toplevel.app_id == top.app_id:
							app_id += 1
					if app_id == 1:
						top_levels[top_index] = toplevel.app_id
				else:
					top_levels[top_index] = toplevel.app_id
			except:
				pass
		s = 0
		for top in top_levels:
			if type(top) == str:
				pass
			elif type(top) == list:
				if toplevel in top:
					top.remove(toplevel)
					if len(top) == 1:
						top_levels.pop(s)
						top_levels.insert(s,top[0])
					break
			elif top == toplevel:
				top_levels.remove(toplevel)
				break
			s += 1
		update_panel_tasks()

def get_battery_number():
	try:
		bat_num = False
		dirs = os.listdir("/sys/class/power_supply/")
		for d in dirs:
			if "BAT" in d:
				bat_num = d
				break
		return bat_num
	except:
		return False

def get_battery_info():
	try:
		if bat_number:
			bat_percent = False
			charging = False
			out_ = subprocess.check_output(["cat","/sys/class/power_supply/{}/capacity".format(bat_number)])
			bat_percent = out_.decode("utf-8")
			out_ = subprocess.check_output(["cat","/sys/class/power_supply/{}/status".format(bat_number)])
			charging_out = out_.decode("utf-8")
			if charging_out[:-1] == "Discharging":
				charging = False
			else:
				charging = True
			return (int(bat_percent),charging)
		else:
			return False
	except:
		return False


def get_wifi_interface():
	try:
		interface = False
		out_ = subprocess.check_output(["iw","dev"])
		out_ = out_.decode("utf-8")
		out_ = out_.split("\n")
		for o in out_:
			if "Interface" in o:
				interface = o.split(" ")[-1]
				break
		return interface
	except:
		return False

def get_sound_info():
	try:
		out_ = subprocess.check_output(["/usr/bin/bash", "-c","LC_ALL=C pactl list sinks"])
		out_ = out_.decode("utf-8")
		out_ = out_.split("\n")
		for o in out_:
			o = o.replace(" ","")
			o = o.replace("\t","")
			a = o.split(":")
			if a[0] == "Volume":
				b = a[2]
				b = b.split("/")[1]
				b = b.split("%")[0]
				percent = int(b)
			if a[0] == "Mute":
				if a[1] == "yes":
					is_mute = True
				else:
					is_mute = False				
		return (percent, is_mute)
	except:
		return False

def get_backlight_info():
	try:
		out_ = subprocess.check_output(["/usr/bin/bash", "-c","LC_ALL=C light -G"])
		out_ = out_.decode("utf-8")
		out_ = out_.split("\n")[0]
		out_ = out_.split(".")[0]
		return int(out_)
	except:
		return False

def get_wifi_info():
	try:
		if wifi_interface:
			ssid = ""
			signal = ""
			out_ = subprocess.check_output(["iw","dev",wifi_interface,"link"])
			out_ = out_.decode("utf-8")
			out_ = out_.split("\n")
			for o in out_:
				if "SSID:" in o:
					ssid = o.split(" ")[-1]
				elif "signal:" in o:
					signal =  o.split(" ")[1]
			return (ssid,int(150+(int(signal))*(5/3)))
		else:
			return False
	except:
		return "No Connection"

def get_bluetooth_info():
	try:
		out_ = subprocess.check_output(["bluetoothctl","show"],timeout=0.05)
		out_ = out_.decode("utf-8")
		out_ = out_.split("\n")
		for o in out_:
			if "Powered:" in o:
				o = o.split(": ")[1]
				return o
		return False
	except:
		return False

def get_notifications():
	try:
		out_ = subprocess.check_output(["swaync-client","-c"],timeout=0.05)
		out_ = out_.decode("utf-8")
		return out_
	except:
		return ""

def time_controls():
	if "wifi" in settings["widgets_left"] or "wifi" in settings["widgets_right"]:
		wifi_info = get_wifi_info()
		if wifi_info:
			for panel in panel_wins:
				panel.update_wifi(wifi_info)
		else:
			for panel in panel_wins:
				panel.wifi_btn.hide()
	if "bluetooth" in settings["widgets_left"] or "bluetooth" in settings["widgets_right"]:
		bluetooth_info = get_bluetooth_info()
		if bluetooth_info:
			for panel in panel_wins:
				if not panel.blue_btn.is_visible():
					panel.blue_btn.show()
				panel.update_bluetooth(bluetooth_info)
		else:
			for panel in panel_wins:
				panel.blue_btn.hide()
				panel.blue_mix.hide()
				panel.blue_on_off.hide()
	if "backlight" in settings["widgets_left"] or "backlight" in settings["widgets_right"]:
		backlight_info = get_backlight_info()
		if backlight_info:
			for panel in panel_wins:
				panel.update_backlight(backlight_info)
		else:
			for panel in panel_wins:
				panel.backlight_btn.hide()
	if "sound" in settings["widgets_left"] or "sound" in settings["widgets_right"]:
		sound_info  = get_sound_info()
		if sound_info:
			for panel in panel_wins:
				panel.update_sound(sound_info)
		else:
			for panel in panel_wins:
				panel.sound_btn.hide()
	if "battery" in settings["widgets_left"] or "battery" in settings["widgets_right"]:
		battery_info = get_battery_info()
		if battery_info:
			for panel in panel_wins:
				panel.update_battery(battery_info)
		else:
			for panel in panel_wins:
				panel.battery_btn.hide()
	if "clock" in settings["widgets_left"] or "clock" in settings["widgets_right"]:
		for panel in panel_wins:
			panel.update_clock()
	if "notifications" in settings["widgets_left"] or "clock" in settings["widgets_right"]:
		for panel in panel_wins:
			# bildirimleri kayıt aktif ise dbus dinlenecek ve ayguciye kayıt edilmesi için gönderilecek.
			if settings["notification_save"] == 1:
				DBusGMainLoop(set_as_default=True)
				bus = dbus.SessionBus()
				bus.add_match_string_non_blocking("eavesdrop=true, interface='org.freedesktop.Notifications', member='Notify'")
				bus.add_message_filter(new_notifications)
			notifications_info  = get_notifications()
			panel.update_notifications(notifications_info)
	return True

def read_settings():
	if os.path.exists(mpanel_settings):
		f_set = GLib.KeyFile()
		f_set.load_from_file(mpanel_settings, GLib.KeyFileFlags.NONE)
		settings["position"] = f_set.get_string("panel","position")
		settings["space"] = f_set.get_integer("panel","space")
		settings["w_space"] = f_set.get_integer("panel","w_space")
		settings["icon_size"] = f_set.get_integer("panel","icon_size")
		settings["line_size"] = f_set.get_integer("panel","line_size")
		settings["systray_size"] = f_set.get_integer("panel","systray_size")
		settings["show_all_monitors"] = f_set.get_integer("panel","show_all_monitors")
		settings["clock_format"] = f_set.get_string("panel","clock_format")
		settings["group_tasks"] = f_set.get_integer("panel","group_tasks")
		settings["widgets_left"] = f_set.get_string_list("panel","widgets_left")
		settings["widgets_right"] = f_set.get_string_list("panel","widgets_right")
		settings["pinned_apps"] = f_set.get_string_list("panel","pinned_apps")
		settings["notification_char_size"] = f_set.get_integer("notification","char_size")
		settings["notification_font"] = f_set.get_string("notification","font")
		settings["notification_time_out"] = f_set.get_integer("notification","time_out")
		settings["notification_save"] = f_set.get_integer("notification","save")
	else:
		write_settings(None,None)


def write_settings(win,x):
	mpanel_dir = os.path.expanduser("~/.config/")
	if not os.path.exists(mpanel_dir):
		os.mkdir(mpanel_dir)
	f_set = GLib.KeyFile()
	f_set.set_string("panel","position",settings["position"])
	f_set.set_integer("panel","space",settings["space"])
	f_set.set_integer("panel","w_space",settings["w_space"])
	f_set.set_integer("panel","icon_size",settings["icon_size"])
	f_set.set_integer("panel","line_size",settings["line_size"])
	f_set.set_integer("panel","systray_size",settings["systray_size"])
	f_set.set_integer("panel","show_all_monitors",settings["show_all_monitors"])
	f_set.set_string("panel","clock_format",settings["clock_format"])
	f_set.set_integer("panel","group_tasks",settings["group_tasks"])
	f_set.set_string_list("panel","widgets_left",settings["widgets_left"])
	f_set.set_string_list("panel","widgets_right",settings["widgets_right"])
	f_set.set_string_list("panel","pinned_apps",settings["pinned_apps"])
	f_set.set_integer("notification","char_size", settings["notification_char_size"])
	f_set.set_string("notification","font", settings["notification_font"])
	f_set.set_integer("notification","time_out", settings["notification_time_out"])
	f_set.set_integer("notification","save", settings["notification_save"])
	f_set.save_to_file(mpanel_settings)
	if win:
		Gtk.main_quit()

def new_notifications(bus, message):
	if type(message) == dbus.lowlevel.MethodCallMessage:
		l = []
		for m in message.get_args_list():
			if type(m) == dbus.String:
				l.append(str(m))
		icon = ""
		title = l[2]
		text = l[3]
		if l[1] != "":
			icon = l[1]
		elif l[0]:
			icon = l[0]
		# ayguci için bildirim dict nesnesinin hazırlanması
		notify_obj = {"title" : title,"body"  : text,"mode"  : "save"}
		ret = ayguci_post("notify",notify_obj)
		# ayguci geri dönüş: mode saved olmalı
		print("ayguci:",ret)

# ayguci e json veri post etme işlevi
def ayguci_post(route, data):
	api = f"http://localhost:{ayguci_port}/api/{route}"
	aheaders = {
		"Accept"       : "application/json",
		"content-type" : "application/json",
	}
	resp = requests.post(api, data=json.dumps(data), headers=aheaders)
	if resp.json():
		if "err" in resp.json():
			err = resp.json()["err"]
			print("ERROR:",arg,err)
			return err
		return resp.json()["data"]

def render():
	for panel in panel_wins:
		panel.systray_store.clear()
		for key, item in reversed(systray_items.items()):
			name, path = key.split('/', 1)
			if item['Status'] != 'Passive':
				pb = get_icon_in_theme(item["IconName"])
				panel.systray_store.append([pb,name,item["Menu"]])
		if settings["position"] == "top" or settings["position"] == "bottom":
			panel.systray_iv.set_columns(len(systray_items.keys()))
		else:
			panel.systray_iv.set_columns(1)

def get_item_data(conn, sender, path):
	def callback(conn, red, user_data=None):
		args = conn.call_finish(red)
		systray_items[sender + path] = args[0]
		render()

	conn.call(
		sender,
		path,
		'org.freedesktop.DBus.Properties',
		'GetAll',
		GLib.Variant('(s)', ['org.kde.StatusNotifierItem']),
		GLib.VariantType('(a{sv})'),
		Gio.DBusCallFlags.NONE,
		-1,
		None,
		callback,
		None,
	)

def on_signal(conn, sender, path, interface, signal, params, invocation, user_data=None):
	if signal == 'NameOwnerChanged':
		if params[2] != '':
			return
		keys = [key for key in systray_items if key.startswith(params[0] + '/')]
		if not keys:
			return
		for key in keys:
			del systray_items[key]
		render()
	elif sender + path in systray_items:
		get_item_data(conn, sender, path)

def on_name_lost(conn, name, user_data=None):
	print('Could not aquire name {}.\nIs some other service blocking it?'.format(name))

def on_bus_acquired(conn, name, user_data=None):
	for interface in NODE_INFO.interfaces:
		if interface.name == name:
			conn.register_object('/StatusNotifierWatcher', interface, on_call)

	def signal_subscribe(interface, signal):
		conn.signal_subscribe(
			None,  # sender
			interface,
			signal,
			None,  # path
			None,
			Gio.DBusSignalFlags.NONE,
			on_signal,
			None,  # user_data
		)

	signal_subscribe('org.freedesktop.DBus', 'NameOwnerChanged')
	for signal in ['NewAttentionIcon','NewIcon','NewIconThemePath','NewStatus','NewTitle']:
		signal_subscribe('org.kde.StatusNotifierItem', signal)

def on_call(conn, sender, path, interface, method, params, invocation, user_data=None):
	props = {
		'RegisteredStatusNotifierItems': GLib.Variant('as', systray_items.keys()),
		'IsStatusNotifierHostRegistered': GLib.Variant('b', True),
	}
	if method == 'Get' and params[1] in props:
		invocation.return_value(GLib.Variant('(v)', [props[params[1]]]))
		conn.flush()
	if method == 'GetAll':
		invocation.return_value(GLib.Variant('(a{sv})', [props]))
		conn.flush()
	elif method == 'RegisterStatusNotifierItem':
		if params[0].startswith('/'):
			path = params[0]
		else:
			path = '/StatusNotifierItem'
		get_item_data(conn, sender, path)
		invocation.return_value(None)
		conn.flush()


if __name__ == '__main__':
	read_settings()
	first_start = True
	top_levels = [*settings["pinned_apps"]]
	not_windows = []
	panel_wins = []
	systray_items = {}
	screen = Gdk.Display.get_default()
	icon_theme = Gtk.IconTheme.get_default()
	if settings["show_all_monitors"]:
		monitors = screen.get_n_monitors()
	else:
		monitors = 1
	for monitor in range(0,monitors):
		active_monitor = screen.get_monitor(monitor)
		SIZE = active_monitor.get_geometry()
		win = MPanel(SIZE)
		GtkLayerShell.init_for_window(win)
		GtkLayerShell.auto_exclusive_zone_enable(win)
		if settings["position"] == "left":
			GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.TOP, 1)
			GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.BOTTOM, 1)
			GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.LEFT, 1)
		elif settings["position"] == "right":
			GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.TOP, 1)
			GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.BOTTOM, 1)
			GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.RIGHT, 1)
		elif settings["position"] == "top":
			GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.TOP, 1)
			GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.LEFT, 1)
			GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.RIGHT, 1)
		elif settings["position"] == "bottom":
			GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.LEFT, 1)
			GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.BOTTOM, 1)
			GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.RIGHT, 1)
		win.show_all()
		panel_wins.append(win)
		win.connect("destroy", write_settings)
	wifi_interface = get_wifi_interface()
	bat_number = get_battery_number()
	context = Context()
	context.connect('toplevel_closed', on_toplevel_closed)
	context.connect('toplevel_new', on_toplevel_new)
	context.connect('toplevel_synced', on_toplevel_synced)
	GObject.timeout_add(1000, time_controls)

	update_panel_tasks()
	if "systray" in settings["widgets_left"] or "systray" in settings["widgets_right"]:
		NODE_INFO = Gio.DBusNodeInfo.new_for_xml("""<?xml version="1.0" encoding="UTF-8"?><node><interface name="org.kde.StatusNotifierWatcher"><method name="RegisterStatusNotifierItem"><arg type="s" direction="in"/></method><property name="RegisteredStatusNotifierItems" type="as" access="read"></property><property name="IsStatusNotifierHostRegistered" type="b" access="read"></property></interface></node>""")
		owner_id = Gio.bus_own_name(
			Gio.BusType.SESSION,
			NODE_INFO.interfaces[0].name,
			Gio.BusNameOwnerFlags.NONE,
			on_bus_acquired,
			None,
			on_name_lost,
		)
		mainloop = GLib.MainLoop()#gi.repository.GLib.MainLoop()
		mainloop.run()
	Gtk.main()

